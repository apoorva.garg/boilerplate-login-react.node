const jwt = require('jsonwebtoken')
const User = require('../models/user')

module.exports = async (req, res, next) => {
    const authHeader = req.get('Authorization')
    if (!authHeader) {
        const error = new Error('Not authenticated.');
        error.statusCode = 401;
        throw error;
    }
    let decodedToken
    const token = authHeader.split(' ')[1]
    try {
        decodedToken = jwt.verify(token, 'somesuperuser');
    } catch (error) {
        error.statusCode = 500;
        throw error;
    }
    if (!decodedToken) {
        const error = new Error('Not authenticated.');
        error.statusCode = 401;
        throw error;
    }
    const user = await User.findByToken(token)
    if (!user) {
        const error = new Error('Not authenticated.');
        error.statusCode = 401;
        throw error;
    }
    req.token = user.token;
    req.user = user;
    next();
}