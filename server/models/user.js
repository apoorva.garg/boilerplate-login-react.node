const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const saltRound = 12;

const userSchema = mongoose.Schema({
    name: {
        type: String,
        maxlength: 50
    },
    email: {
        type: String,
        trim: true,
        unique: 1
    },
    password: {
        type: String,
        minlength: 5
    },
    lastname: {
        type: String,
        maxlength: 50
    },
    role: {
        type: Number,
        default: 0
    },
    image: String,
    token: {
        type: String,
    },
    tokenExp: {
        type: Number,
    }
})

userSchema.pre('save', function (next) {
    let user = this;

    if (user.isModified('password')) {
        bcrypt.genSalt(saltRound, function (err, salt) {
            if (err) {
                const error = new Error('Something went wrong')
                error.statusCode = 500;
                return error;
            }
            bcrypt.hash(user.password, salt, function (err, hash) {
                if (err) {
                    const error = new Error('Something went wrong')
                    error.statusCode = 500;
                    return error;
                }
                user.password = hash
                next();
            })
        })
    } else {
        next();
    }
})


userSchema.methods.comparePassword = async function (password) {
    const isMatch = await bcrypt.compare(password, this.password)
    return isMatch
}

userSchema.methods.generateToken = async function () {
    let user = this;
    let token = jwt.sign({
        email: user.email,
        userId: user._id.toHexString()
    }, 'somesuperuser', { expiresIn: '1h' })
    user.token = token;
    const userToken = await user.save();
    if (!userToken) {
        return false;
    }
    return userToken
}


userSchema.statics.findByToken = async function (token) {
    let user = this;

    const oldUser = await user.findOne({ token: token })
    if (!oldUser) {
        const error = new Error('user not found')
        error.statusCode = 401;
        return error;
    }
    return oldUser
}

module.exports = mongoose.model('User', userSchema);