const User = require('../models/user')

exports.register = async (req, res, next) => {
    try {

        const user = new User(req.body);
        const result = await user.save()
        res.status(201).json({ success: true })

    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500;
        }
        next(error);
    }
}

exports.login = async (req, res, next) => {
    try {
        const user = await User.findOne({ email: req.body.email })
        if (!user) {
            const error = new Error('Email not found')
            error.statusCode = 401;
            throw error
        }

        const isEqual = await user.comparePassword(req.body.password);
        if (!isEqual) {
            const error = new Error('Password not Matched')
            error.statusCode = 401;
            throw error
        }
        const userToken = await user.generateToken();
        if (!userToken) {
            const error = new Error('Something went wrong.')
            error.statusCode = 500;
            throw error
        }

        res.status(200).json({ success: true, token: userToken.token })

    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500
        }
        next(error);
    }
}

exports.auth = async (req, res, next) => {
    try {
        res.status(200).json({
            _id: req.user._id,
            isAdmin: req.user.role === 0 ? false : true,
            isAuth: true,
            email: req.user.email,
            name: req.user.name,
            lastname: req.user.lastname,
            role: req.user.role,
            image: req.user.image,
        })

    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500
        }
        next(error);
    }
}

exports.logout = async (req, res, next) => {
    try {

        const user = await User.findOneAndUpdate({ _id: req.user._id }, { token: "" });
        if (!user) {
            const error = new Error('No user found')
            error.statusCode = 401;
            throw error
        }

        res.status(200).json({ success: true })


    } catch (error) {
        if (!error.statusCode) {
            error.statusCode = 500
        }
        next(error);
    }
}

