const express = require('express');

const userController = require('../controllers/user');
const isAuth = require('../middleware/is_auth')


const router = express.Router();

router.post('/register', userController.register);
router.post('/login', userController.login);
router.get('/auth', isAuth, userController.auth);
router.get('/logout', isAuth, userController.logout);

module.exports = router;